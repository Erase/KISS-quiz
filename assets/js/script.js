"use strict";

//~ Initialize reveal.js
Reveal.initialize({
	slideNumber: true,
	overview: false,
	controls: false,
	center:false,
	keyboard: false,
	help: false,
	dependencies: [
		{ src: 'vendor/reveal.js/plugin/markdown/marked.js' },
		{ src: 'vendor/reveal.js/plugin/markdown/markdown.js' },
		{ src: 'vendor/reveal.js/plugin/notes/notes.js', async: true },
		{ src: 'vendor/reveal.js/plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
	]
});

$(window).ready(function(){
	/**
	* function loadNext
	* Load the next question
	* @param i : number of the question to call
	**/
	function loadNext(i){
		$.ajax({
			url: 'ajax/questions.php',
			dataType: 'json',
		})
		.done(function(data) {
			//~ End of quizz
			if(data.status === 'end'){
				$('#q'+i).html(data.result);
			}else{
				//~ Insert next question to DOM
				var question = data.question;
				var _p = data.propositions;
		
				var propositions = "<ul class='no-puce'>";

				//~ Loading proposals
				$.each(_p, function(index, row) {
					propositions += "<li><input type='radio' id='q"+data.c+"-"+index+"' name='q"+data.c+"' value='q"+data.c+"-"+index+"' /><label for='q"+data.c+"-"+index+"'>"+row+"</li>";
				});

				//~ Add clic event
				$('#q'+i).html(question+propositions+"</ul>").find('input').change(function(){
					i++;
					var r = $(this).val();
					$.ajax({
						url: 'ajax/questions.php',
						data: {r: r},
					})
					.done(function() {
						loadNext(i);
						Reveal.next();
					})
				});
			}
		});
	}

	//~ Load first question
	loadNext(0);
	
});