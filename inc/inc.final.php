<?php
	
	include '../inc/inc.banque.php';

	$n_questions = count($_questions);

	//~ Levels
	/*
		$n_error : number of errors
		array(
			'min' 		=> 'minimum number of errors',
			'max' 		=> 'maximum number of errors',
			'message'	=> 'Message to be displayed'
		)
	*/
	$_levels = array(
		array(
			'min'		=>	0,
			'max'		=>	0,
			'message'	=>	'<h2>Félicitations !</h2><h4>Vous n\'avez fait aucune erreur sur un total de '.$n_questions.' questions.</h4>'
		),
		array(
			'min'		=>	1,
			'max'		=>	3,
			'message'	=>	'<h2>Pas mal !</h2><h4>Vous avez fait '.$n_error.' erreur'.($n_error > 1 ? "s" : "").' sur un total de '.$n_questions.' questions.</h4>'
		),
		array(
			'min'		=>	4,
			'max'		=>	$n_questions,
			'message'	=>	'<h2>Outch !</h2><h4>Vous avez fait '.$n_error.' erreurs sur un total de '.$n_questions.' questions.</h4>'
		)
	);
