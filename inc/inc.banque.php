<?php 
	/*
	
		$_questions : Questions array

		//~ New question
		[0] => array(
			'question'	=>	'Title of the question',
			'propositions'	=> array(
				'response proposal one',
				'response proposal two',
				'response proposal three'
				' etc.'
			),
			'reponse'	=> 'index of good response' (0 = first proposal, 1 = second proposal, etc.)
		)

	*/
	$_questions = array(
		0	=>	array(
			'question'		=> '<h4>Quel est cet animal ?</h4><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Lil_Bub_2013_%28crop_for_thumb%29.jpg/590px-Lil_Bub_2013_%28crop_for_thumb%29.jpg" width="200"/><br>',
			'propositions' 	=> array(
				'un chien',
				'un chat',
				'une girafe',
				'aucun de ces animaux'
			),
			'reponse'		=> 1
		),
		1	=>	array(
			'question'		=> '<h4>Qu\'est ce que <code>$_SERVER</code> ?</h4>',
			'propositions' 	=> array(
				'une supervariable',
				'une variable superglobale',
				'un équivalent de <code>$_GLOBAL</code>',
				'un poney'
			),
			'reponse'		=> 1
		),
		2	=>	array(
			'question'		=> '<h4>De quelle couleur est le mot suivant : <span style="color:red;font-weight:bold;">VERT</span> ?</h4>',
			'propositions' 	=> array(
				'arc-en-ciel',
				'jaune',
				'vert',
				'rouge'
			),
			'reponse'		=> 3
		),
		3	=>	array(
			'question'		=> '<h4>Que va afficher le code suivant ?</h4><pre><code>&lt;?php
	echo strtoupper("Toto");
?&gt;</code></pre>',
			'propositions' 	=> array(
				'toto',
				'TOTO',
				'ToTo',
				'Tutu'
			),
			'reponse'		=> 1
		)
	);