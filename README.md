[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

KISS and fluid quiz with AJAX
----------

A stupid and simple quiz with [reveal.js](https://github.com/hakimel/reveal.js/)

Simply editable questions. Dynamic loading, questions by questions, with AJAX.

## Démo ?
Yep, **[here](http://demo.green-effect.fr/KISS-quiz/)**

## Installation and personalization
- ```git clone``` this repository
- open file ```/inc/inc.banque.php```
- modify/add your questions
- open file ```/inc/inc.final.php```
- modify the final text according to the number of errors
- that's all !

If you want to change the global theme, you can modify the style sheet called line 17 of the ```index.php``` file. The different themes are those provided by reveal.js, found in the ```vendor / reveal.js / css / theme``` directory


## License

Apart from the different licenses specific to the tools used, the rest of the code is licensed [Creative Commons BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/)

Icons made by [Freepik](http://www.freepik.com), licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/).

## Auteur

[A simple gardener developper](https://www.green-effect.fr/) :) with a little free time and no pretension - contact_at_green-effect.fr
