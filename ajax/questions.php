<?php
	session_start();

	//~ New user, new response array
	if(!array_key_exists('response', $_SESSION)){
		$_SESSION['response'] = array();
	}

	//~ Processing an answer
	if($_GET && array_key_exists('r', $_GET)){
		$_SESSION['response'][$_SESSION['current_question']] = $_GET['r'];
	}

	include '../inc/inc.banque.php';

	//~ Size & count
	$n_questions    = count($_questions) - 1;

	//~ End quizz ?
	$n_response 	= count($_SESSION['response']) -1;
	
	if($n_questions != $n_response){
		//~ New question
		$_SESSION['current_question'] = rand(0, $n_questions);

		//~ Already answered?
		while(array_key_exists($_SESSION['current_question'], $_SESSION['response'])){
			$_SESSION['current_question'] = rand(0, $n_questions);
		}

		//~ Return question
		$return = array_merge($_questions[$_SESSION['current_question']], array(
			'c' => $_SESSION['current_question']
		));

		header('Content-Type: application/json');
		echo json_encode($return);
		exit;
	}else{
		// ~ End
		$n_error = 0;
		foreach ($_SESSION['response'] as $index => $reponse) {
			if(array_key_exists($index, $_questions)){
				if($reponse !== "q".$index."-".$_questions[$index]['reponse']){
					$n_error++;
				}
			}
		}

		//~ Calculation of correct answers
		include '../inc/inc.final.php';

		foreach ($_levels as $key => $level) {
			if($n_error >= $level['min'] && $n_error <= $level['max']){
				//~ Return result
				header('Content-Type: application/json');
				echo json_encode(array(
					'status' => 'end',
					'result' => $level['message']
				));
				exit;
			}
		}
		//~ Error in the determination of the result
		header('Content-Type: application/json');
		echo json_encode(array(
			'status' => 'end',
			'result' => 'Une erreure est survenue dans le calcul de votre résultat.'
		));
		exit;
	}

?>