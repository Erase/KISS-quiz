<?php
	session_start();

	//~ Responses array
	$_SESSION['response'] = array();
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<title>Questionnaire</title>

		<!-- reveal.js CSS theme -->
		<link rel="stylesheet" href="vendor/reveal.js/css/reveal.css">
		<link rel="stylesheet" href="vendor/reveal.js/css/theme/league.css">

		<!-- CSS -->
		<link rel="stylesheet" href="assets/css/style.css">

		<!-- Theme used for syntax highlighting of code -->
		<link rel="stylesheet" hraffichagef="vendor/reveal.js/lib/css/zenburn.css">

		<!-- Printing and PDF exports -->
		<script>
			var link = document.createElement('link');
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = window.location.search.match( /print-pdf/gi ) ? 'vendor/reveal.js/css/print/pdf.css' : 'vendor/reveal.js/css/print/paper.css';
			document.getElementsByTagName( 'head' )[0].appendChild( link );
		</script>
	</head>
	<body>
		<div class="reveal">
			<div class="slides">
				<?php 
					include 'inc/inc.banque.php';

					//~ Generates <section> for each questions
					for($i=0; $i < count($_questions) +1; $i++){
						?>
							<section id="q<?php echo $i;?>"></section>
						<?php
					}
				?>
			</div>
		</div>

		<!-- JS lib -->
		<script src="vendor/jquery/3.3.1/jquery-3.3.1.min.js"></script>
		<script src="vendor/reveal.js/lib/js/head.min.js"></script>
		<script src="vendor/reveal.js/js/reveal.js"></script>

		<!-- JS -->
		<script src="assets/js/script.js"></script>
	</body>
</html>
	